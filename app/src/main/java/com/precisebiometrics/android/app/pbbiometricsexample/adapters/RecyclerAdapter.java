package com.precisebiometrics.android.app.pbbiometricsexample.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.precisebiometrics.android.app.pbbiometricsexample.R;

/**
 * Created by sp_developer on 4/1/16.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.DataObjectHolder> {


    private String[] TextString;
    Context mContext ;
    private OnClickListener listener;



    public interface OnClickListener{

        void onClick(String[] item, int position);
    }

    public RecyclerAdapter(Context context , String[] text, OnClickListener listener){
        TextString = text;
        mContext = context;
        this.listener = listener;
    }


    public static class DataObjectHolder extends RecyclerView.ViewHolder{

        private TextView label;
        //private ImageView mImageview;


        public View mView = null ;

        public DataObjectHolder(View itemView) {
            super(itemView);

            mView = itemView;
            label = (TextView) itemView.findViewById(R.id.Title_label);

        }

        public void bind(final String[] items, final OnClickListener listener, final int position){

            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(items,position);
                }
            });
        }
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menu_row_adapter, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerAdapter.DataObjectHolder holder, final int position) {

        holder.label.setText(TextString[position]);

        holder.bind(TextString,listener,position);

    }

    @Override
    public int getItemCount() {
        return TextString.length;
    }





}
