package com.precisebiometrics.android.app.pbbiometricsexample;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.media.FaceDetector;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.precisebiometrics.android.mtk.api.bio.PBBiometry;
import com.precisebiometrics.android.app.pbbiometricsexample.Util.PhotoCapture_Lab;

import java.io.ByteArrayOutputStream;

public class EnrolleeActivity_2 extends AppCompatActivity {

    private Button NextButton, BackButton, TakePhoto;
    private ImageView photo;
    private static final int TAKE_PHOTO = 100;
    private static final int MAX_FACES = 1;


    private Bitmap cameraBitmap = null;

    private static Bitmap codec(Bitmap src, Bitmap.CompressFormat format,
                                int quality) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        src.compress(format, quality, os);

        byte[] array = os.toByteArray();
        return BitmapFactory.decodeByteArray(array, 0, array.length);



        // Best of quality is 80 and more, 3 is very low quality of image

       // Bitmap bJPGcompress = codec(bitmap, Bitmap.CompressFormat.JPEG, 3);


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enrollee_activity_2);

        NextButton = (Button) findViewById(R.id.nxt_bnt);
        BackButton = (Button) findViewById(R.id.prev_btn);

        TakePhoto = (Button) findViewById(R.id.takephoto_btn);
        photo = (ImageView) findViewById(R.id.imageview);


        TakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, TAKE_PHOTO);
            }
        });

        NextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // enrollManager(v);
                Intent i = new Intent(EnrolleeActivity_2.this, EnrolleeActivity_3.class);
                startActivity(i);

                overridePendingTransition(R.anim.activity_in, R.anim.activity_out);

            }
        });


        BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);

            }
        });


    }
    public void enrollManager(View view) {
        PBBiometry.manageDeviceOwnerFingers(getApplication());
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try{

            if (data == null){

                return;
            }

            if(requestCode == TAKE_PHOTO){
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Bitmap bitmap = (Bitmap) data.getExtras().get("data");

                // Bitmap resized = Bitmap.createScaledBitmap(bitmap, 78, 97, true);


                photo.setImageBitmap(bitmap);

                // add photo to singleton class
                PhotoCapture_Lab.getInstance(EnrolleeActivity_2.this).addDetails(bitmap);


            }


        } catch (Exception e){
            e.printStackTrace();
        }

    }


    //    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//
//        switch (id) {
//
//            case android.R.id.home :
//                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
//
//
//        }
//        return super.onOptionsItemSelected(item);
//    }


    private void processCameraImage(Intent intent) {
        setContentView(R.layout.detect_layout);

        final Button detectFace = (Button) findViewById(R.id.detect_face);
        detectFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                detectFaces();
            }
        });
        //   ((Button)findViewById(R.id.detect_face)).setOnClickListener(btnClick);

        ImageView imageView = (ImageView) findViewById(R.id.image_view);

        Bitmap bitmap = (Bitmap) intent.getExtras().get("data");

        imageView.setImageBitmap(bitmap);
    }

    private void detectFaces() {
        if (null != cameraBitmap) {
            int width = cameraBitmap.getWidth();
            int height = cameraBitmap.getHeight();

            FaceDetector detector = new FaceDetector(width, height, MAX_FACES);
            FaceDetector.Face[] faces = new FaceDetector.Face[MAX_FACES];

            Bitmap bitmap565 = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            Paint ditherPaint = new Paint();
            Paint drawPaint = new Paint();

            ditherPaint.setDither(true);
            drawPaint.setColor(Color.RED);
            drawPaint.setStyle(Paint.Style.STROKE);
            drawPaint.setStrokeWidth(2);

            Canvas canvas = new Canvas();
            canvas.setBitmap(bitmap565);
            canvas.drawBitmap(cameraBitmap, 0, 0, ditherPaint);

            int facesFound = detector.findFaces(bitmap565, faces);
            PointF midPoint = new PointF();
            float eyeDistance = 0.0f;
            float confidence = 0.0f;

            Log.i("FaceDetector", "Number of faces found: " + facesFound);

            if (facesFound > 0) {
                for (int index = 0; index < facesFound; ++index) {
                    faces[index].getMidPoint(midPoint);
                    eyeDistance = faces[index].eyesDistance();
                    confidence = faces[index].confidence();

                    Log.i("FaceDetector",
                            "Confidence: " + confidence +
                                    ", Eye distance: " + eyeDistance +
                                    ", Mid Point: (" + midPoint.x + ", " + midPoint.y + ")");

                    canvas.drawRect((int) midPoint.x - eyeDistance,
                            (int) midPoint.y - eyeDistance,
                            (int) midPoint.x + eyeDistance,
                            (int) midPoint.y + eyeDistance, drawPaint);
                }
            }

            //String filepath = Environment.getExternalStorageDirectory() + "/facedetect" + System.currentTimeMillis() + ".jpg";

//            try {
//                FileOutputStream fos = new FileOutputStream(filepath);
//
//                bitmap565.compress(Bitmap.CompressFormat.JPEG, 90, fos);
//
//                fos.flush();
//                fos.close();
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }

        }
    }
}
