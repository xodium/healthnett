package com.precisebiometrics.android.app.pbbiometricsexample.Util;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;

import com.precisebiometrics.android.app.pbbiometricsexample.R;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class DatePicker extends DialogFragment {
    Date mNewDate , NewDate;
    public static final String Key_2 = "Registration Fragment";

    public DatePicker() {
        // Required empty public constructor
    }

    private void sendResult(int resultCode) {
        if(getTargetFragment() == null)
            return;

        Intent i = new Intent();
        i.putExtra(Key_2, NewDate);
        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, i);

    }

    public static DatePicker newInstance(Date date) {
        Bundle args = new Bundle();
        args.putSerializable("KEY_1", date );

        DatePicker fragment = new DatePicker();
        fragment.setArguments(args);

        return fragment;

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Date mDate = (Date)getArguments().getSerializable("KEY_1");

        // create a Calendar to get the yr,month,day
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(mDate);

        int yr = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        mNewDate = new GregorianCalendar(yr,month,day).getTime();


        View v = getActivity().getLayoutInflater().inflate(R.layout.datepicker_dialog,null);

        android.widget.DatePicker datePicker = (android.widget.DatePicker)v.findViewById(R.id.dialog_date_picker);
        datePicker.init(yr, month, day, new android.widget.DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(android.widget.DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                // Translate year, month, day into Date object using GregorianCalendar

                NewDate = new GregorianCalendar(year,monthOfYear,dayOfMonth).getTime();

            }
        });
        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle("Date Of Birth")
                .setNegativeButton("Cancel", null)
                .setPositiveButton("Set", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendResult(Activity.RESULT_OK);
                    }
                })
                .create();
    }



}
