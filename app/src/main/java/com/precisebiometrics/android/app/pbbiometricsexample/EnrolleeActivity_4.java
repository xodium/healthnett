package com.precisebiometrics.android.app.pbbiometricsexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class EnrolleeActivity_4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enrollee_activity_4);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);

    }
}
