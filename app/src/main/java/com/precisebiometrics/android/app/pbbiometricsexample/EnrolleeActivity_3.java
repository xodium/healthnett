package com.precisebiometrics.android.app.pbbiometricsexample;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcel;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.precisebiometrics.android.mtk.api.PBInitializedCallback;
import com.precisebiometrics.android.mtk.api.bio.PBBiometry;
import com.precisebiometrics.android.mtk.api.bio.PBBiometryException;
import com.precisebiometrics.android.mtk.biometrics.PBBiometryEnrollConfig;
import com.precisebiometrics.android.mtk.biometrics.PBBiometryFinger;
import com.precisebiometrics.android.mtk.biometrics.PBBiometryImage;
import com.precisebiometrics.android.mtk.biometrics.PBBiometryUser;
import com.precisebiometrics.android.app.pbbiometricsexample.Util.PhotoCapture_Lab;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;
import model.PhotoCapture;
import model.UserCapture;
import persistence.UserDBHandler;

public class EnrolleeActivity_3 extends AppCompatActivity {

    PBBiometry bio;
    private Boolean isComplete = false;
    private ImageView rightIndex, rightThumb, leftIndex, leftThumb;
    private Button scanRightIndex, scanRightThumb, scanLeftIndex, scanLeftThumb;
    private LocalDatabase localDB;
    private UserDBHandler userDBHandler = new UserDBHandler(this);
    private int userID = 1;
    private static final String FINGER_RI = "rightIndex";
    private static final String FINGER_RT = "rightThumb";
    private static final String FINGER_LI = "leftIndex";
    private static final String FINGER_LT = "leftThumb";
    private CheckBox checkboxRightIndex, checkboxRightThumb, checkboxLeftIndex, checkboxLeftThumb;
    private TextView textLeftIndex, textLeftThumb, textRightIndex, textRightThumb;

    public Bitmap displayPrintImage(String fingerPos){

        //byte[] imageByte = userDBHandler.getFingerPrintByte(fingerPos);
        UserCapture userCapture = userDBHandler.getFingerCapture(fingerPos);
        BitmapFactory.Options options = new BitmapFactory.Options();
        byte[] imageByte = userCapture.getFingerPrint();
        Bitmap fingerBitmap = null;

        if(imageByte != null) {
            fingerBitmap = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length, options);
            Toast.makeText(EnrolleeActivity_3.this, "The byte[] is " + imageByte, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(EnrolleeActivity_3.this, fingerPos + " Byte Array is Empty", Toast.LENGTH_SHORT).show();
        }
        return fingerBitmap;
    }

    private void Enrollee_Verification_Dialog(Bitmap image){
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.activity_enrollee_activity_4, null);
        CircleImageView circleImageView = (CircleImageView)v.findViewById(R.id.profile_image);
        final ImageView rightIndex = (ImageView)v.findViewById(R.id.image_right_index);
        final ImageView rightThumb = (ImageView)v.findViewById(R.id.image_right_thumb);
        final ImageView leftIndex = (ImageView)v.findViewById(R.id.image_left_index);
        final ImageView leftThumb = (ImageView)v.findViewById(R.id.image_left_thumb);
        circleImageView.setImageBitmap(image);
        BitmapFactory.Options options = new BitmapFactory.Options();
        rightIndex.setImageBitmap(displayPrintImage(FINGER_RI));
        rightThumb.setImageBitmap(displayPrintImage(FINGER_RT));
        leftIndex.setImageBitmap(displayPrintImage(FINGER_LI));
        leftThumb.setImageBitmap(displayPrintImage(FINGER_LT));
        AlertDialog.Builder builder =  new AlertDialog.Builder(EnrolleeActivity_3.this);
        builder.setView(v);
        builder.setPositiveButton("SUBMIT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setNegativeButton("CANCEL",null);
        builder.show();

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalDatabase.initInstance(EnrolleeActivity_3.this);
        setContentView(R.layout.activity_enrollee_activity_3);
        userDBHandler.deleteDB();
        Button nextButton = (Button)findViewById(R.id.nxtbnt_3);
        Button backButton = (Button)findViewById(R.id.backbtn_3);

        checkboxLeftIndex = (CheckBox) findViewById(R.id.checkboxLeftIndex);
        checkboxLeftThumb = (CheckBox) findViewById(R.id.checkboxLeftThumb);
        checkboxRightIndex = (CheckBox) findViewById(R.id.checkboxRightIndex);
        checkboxRightThumb = (CheckBox) findViewById(R.id.checkboxRightThumb);
        nextButton.setEnabled(false);

        textLeftIndex = (TextView) findViewById(R.id.textLeftIndex);
        textLeftThumb = (TextView) findViewById(R.id.textLeftThumb);
        textRightIndex = (TextView) findViewById(R.id.textRightIndex);
        textRightThumb = (TextView) findViewById(R.id.textRightThumb);
        rightIndex = (ImageView) findViewById(R.id.displayRightIndex);
        rightThumb = (ImageView) findViewById(R.id.displayRightThumb);
        leftIndex = (ImageView) findViewById(R.id.displayLeftIndex);
        leftThumb = (ImageView) findViewById(R.id.displayLeftThumb);
        startBiometrics();

        if ((checkboxRightIndex.isChecked() == true) && (checkboxRightThumb.isChecked() == true)
                && (checkboxLeftIndex.isChecked() == true) && (checkboxLeftThumb.isChecked() == true)){
            nextButton.setEnabled(true);
        }

         nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PhotoCapture photoCapture= PhotoCapture_Lab.getInstance(EnrolleeActivity_3.this).getDetails();
                    Enrollee_Verification_Dialog(photoCapture.getPhoto());
                }
            });


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);

            }
        });
        scanRightIndex = (Button) findViewById(R.id.scanRightIndex);
        scanRightThumb = (Button) findViewById(R.id.scanRightThumb);
        scanLeftIndex = (Button) findViewById(R.id.scanLeftIndex);
        scanLeftThumb = (Button) findViewById(R.id.scanLeftThumb);

        scanRightIndex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                insertPrint(FINGER_RI, rightIndex);
            }
        });
        scanRightThumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                insertPrint(FINGER_RT, rightThumb);
            }
        });

        //Left hand
        scanLeftIndex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                insertPrint(FINGER_LI, leftIndex);

            }
        });
        scanLeftThumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                insertPrint(FINGER_LT, leftThumb);

            }
        });
    }
    public void insertPrint(final String finger, final ImageView view){
        final String fingerOnPrint = finger;
        if (bio != null) {
            final EnrollDialog dialog = new EnrollDialog(EnrolleeActivity_3.this, bio);

            dialog.show();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    PBBiometryEnrollConfig config = new PBBiometryEnrollConfig();

                    try {

                        bio.enrollFinger(
                                // Enroll the finger selected in the
                                // GUI
                                getSelectedFinger(fingerOnPrint),
                                // Use the local database for
                                // template storage
                                LocalDatabase.getInstance(),
                                // The GUI functionality is
                                // handled by the enroll dialog.
                                dialog,
                                // We use the recommended 3 sample
                                // enrollment. The GUI is also created
                                // to show the 3 samples, see
                                // EnrollDialog
                                3,
                                // Default configuration is used
                                config,
                                // We use our own GUI so GUI
                                // configuration is useless here
                                null);

                        showInfoDialog("Enroll successful",
                                "Info");
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                isComplete = true;
                            }
                        });
                    } catch (PBBiometryException e) {
                        e.printStackTrace();
                        publishError(e.getErrorDisplayName());
                    } finally {
                        if (dialog != null) {
                            dialog.dismiss();
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    if(isComplete) {
                                        localDB = new LocalDatabase(getApplicationContext());
                                        byte[] fingerPic = new byte[0];
                                        try {
                                            fingerPic = localDB.getTemplate(getSelectedFinger(fingerOnPrint)).getData();
                                        } catch (PBBiometryException e) {
                                            e.printStackTrace();
                                        }
                                        //String fingerPrint64 = Base64.encodeToString(fingerPic, Base64.DEFAULT);
                                        int presentImage = 1;
                                        ImageView[] imageView = dialog.getThumbs(presentImage);
                                        Bitmap bitmap = convertImageviewToBitmap(imageView[presentImage]);
                                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                        byte[] byteArray = stream.toByteArray();
                                        UserCapture userCapture = new UserCapture(userID, byteArray, fingerOnPrint, fingerPic);
                                        userDBHandler = new UserDBHandler(EnrolleeActivity_3.this);
                                        userDBHandler.insertRecord(userCapture);
                                        userID++;
                                        // showInfoDialog(String.valueOf(userDBHandler.getFingerPrintDetails()), fingerOnPrint);
                                        view.setImageResource(R.drawable.successful);
                                        checkBoxSelect(finger);
                                        isComplete = false;
                                    }
                                }
                            });
                        }
                    }
                }

            }).start();
        }
    }
    public void checkBoxSelect(String finger){
        if (finger == FINGER_LI){
            checkboxLeftIndex.setChecked(true);
            textLeftIndex.setText("Left Finger is Present");
        }
        else if (finger == FINGER_LT) {
            checkboxLeftThumb.setChecked(true);
            textLeftThumb.setText("Left Thumb is Present");
        }
        else if (finger == FINGER_RI) {
            checkboxRightIndex.setChecked(true);
            textRightIndex.setText("Right Index is Present");
        }
        else if (finger == FINGER_RT) {
            checkboxRightThumb.setChecked(true);
            textRightThumb.setText("Right Thumb is Present");
        }
    }

    public Bitmap convertImageviewToBitmap(ImageView view){
        Bitmap bmap = ((BitmapDrawable) view.getDrawable()).getBitmap();
        return bmap;
    }
    private PBBiometryFinger getSelectedFinger(String side) {
        // The finger positions here are simply randomly chosen for the example
        // app only.
        if (side == FINGER_LI) {
            return new PBBiometryFinger(PBBiometryFinger.PBFingerPosition.PBFingerPositionLeftIndex,
                    new PBBiometryUser(31));
        } else if (side == FINGER_LT){

            return new PBBiometryFinger(PBBiometryFinger.PBFingerPosition.PBFingerPositionLeftThumb,
                    new PBBiometryUser(31));

        }
        else if (side == FINGER_RI){

            return new PBBiometryFinger(PBBiometryFinger.PBFingerPosition.PBFingerPositionRightIndex,
                    new PBBiometryUser(31));

        }
        else if (side == FINGER_RT){

            return new PBBiometryFinger(PBBiometryFinger.PBFingerPosition.PBFingerPositionRightThumb,
                    new PBBiometryUser(31));

        }
        return null;

    }
    protected void publishError(final String error) {
        showInfoDialog(error, "Try again");
        Log.e("Error message", error);
    }
    private void startBiometrics() {
        // Create the biometrics API object and provide
        // the callback that is informed when the initialization
        // is ready.
        bio = new PBBiometry(EnrolleeActivity_3.this,
                new PBInitializedCallback() {
                    @Override
                    public void uninitialized() {
                        // Note that with the setup in this example app, this  will be
                        // called every time we leave the app, e.g. when moving to
                        // the default GUI of the toolkit.
                        String msg = "Biometrics API connection has been lost";
                        Log.d(Constants.LOG_TAG, msg);
                    }

                    @Override
                    public void initialized() {
                        String val =
                                "Biometrics API hooked" +
                                        "Tactivo Manager is installed.";
                        showInfoDialog(val, "Connected");
                        Log.e(Constants.LOG_TAG, val);
                    }

                    @Override
                    public void initializationFailed() {
                        // notifyBiometricsState(false);
                        String error =
                                "Unable to initialize the Biometrics API.\n" +
                                        "Perhaps the Tactivo Manager is not installed.";
                        showInfoDialog(error, "Error");
                        Log.e(Constants.LOG_TAG, error);
                    }
                });
    }

    protected void showInfoDialog(final String message, final String title) {
        runOnUiThread(new Runnable() {
            @Override
            public void run(){
                AlertDialog.Builder builder = new AlertDialog.Builder(EnrolleeActivity_3.this);//Try using ApplicationContext
                builder.setMessage(message)
                        .setTitle(title)
                        .setPositiveButton("OK", null);
                builder.create().show();
            }
            });


    }

 }
