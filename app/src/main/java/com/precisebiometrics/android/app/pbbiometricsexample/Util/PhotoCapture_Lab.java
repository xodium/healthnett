package com.precisebiometrics.android.app.pbbiometricsexample.Util;

import android.content.Context;
import android.graphics.Bitmap;

import model.PhotoCapture;

/**
 * Created by sp_developer on 4/26/16.
 */
    public class PhotoCapture_Lab {


     private static PhotoCapture_Lab sPhotoCapture_Lab;
     private Context context;
     private PhotoCapture mPhotoCapture;


        private PhotoCapture_Lab(Context context) {

           this.context = context;
           mPhotoCapture = new PhotoCapture();

        }

    public static PhotoCapture_Lab getInstance(Context context) {

        if(sPhotoCapture_Lab == null){
           sPhotoCapture_Lab = new PhotoCapture_Lab(context.getApplicationContext());
        }
        return sPhotoCapture_Lab;
    }


    public PhotoCapture getDetails(){
        return mPhotoCapture;
    }

    public void addDetails(Bitmap bitmap){

          mPhotoCapture.setPhoto(bitmap);
   }


}
