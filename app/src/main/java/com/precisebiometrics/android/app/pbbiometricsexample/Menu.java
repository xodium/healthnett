package com.precisebiometrics.android.app.pbbiometricsexample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.precisebiometrics.android.app.pbbiometricsexample.adapters.RecyclerAdapter;

public class Menu extends AppCompatActivity {

    RecyclerAdapter adapter;
    String[] listItems = {"Enrollee Registration","Ward Selection","Registered Enrollee List","Logout"};

    private RecyclerView mRecyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new RecyclerAdapter(this,listItems, new RecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(String[] item, int position) {

                if(item[position].equals("Enrollee Registration")){

                    Intent intent = new Intent(Menu.this,EnrolleeActivity_1.class);

                    startActivity(intent);

                    //overridePendingTransition(R.anim.activity_in,R.anim.activity_out);

                }

                else if(item[position].equals("Ward Selection")){

                    //Toast.makeText(Menu.this, "Ward Selection was clicked", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(Menu.this,WardSelection_Activity.class);
                    startActivity(intent);

                }

                else if(item[position].equals("Registered Enrollee List")){

                    Toast.makeText(Menu.this,"Registered Enrollee List was clicked", Toast.LENGTH_LONG).show();

                }

                else{

                    Intent intent = new Intent(Menu.this,BioExampleActivity.class);
                    startActivity(intent);

                    finish();

                }

            }
        });

        setContentView(R.layout.activity_menu);

        mRecyclerView = (RecyclerView)findViewById(R.id.list);

        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        // mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(adapter);


    }


}
