package com.precisebiometrics.android.app.pbbiometricsexample;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.precisebiometrics.android.app.pbbiometricsexample.Util.DatePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class EnrolleeActivity_1 extends AppCompatActivity {

    private static final String PICKER = "datePicker";
    private Button btn;
    private static EditText mDob;
    private static  int REQUEST_DATE = 0;
    private static Date newdate;

    // method for formatting the Date object from the date picker to string
    private static String Date_text_formatter(Date date){

        SimpleDateFormat y = new SimpleDateFormat("yyyy");
        SimpleDateFormat m = new SimpleDateFormat("MM");
        SimpleDateFormat d = new SimpleDateFormat("dd");
        SimpleDateFormat D = new SimpleDateFormat("EEEE , MMMM dd, yyyy");

        String x;


      //  x =D.format(date)+"-"+ d.format(date)+"-"+m.format(date)+"-"+y.format(date);
        x = D.format(date);

        return x;
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enrollee_activity_1);



        btn = (Button) findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(EnrolleeActivity_1.this,EnrolleeActivity_2.class);
                startActivity(i);

                overridePendingTransition(R.anim.activity_in,R.anim.activity_out);
            }
        });

        mDob = (EditText)findViewById(R.id.dob);
        mDob.setText(Date_text_formatter(new Date())); // set textfield with present date by default
        mDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // show date picker dialog

                android.app.FragmentManager fm = getFragmentManager();
                DialogFragment datePickerFragment = new DatePickerFragment();
                datePickerFragment.show(fm,PICKER);
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        newdate = (Date)data.getSerializableExtra(DatePicker.Key_2);

        if(resultCode != Activity.RESULT_OK)
            return;
        if(requestCode == REQUEST_DATE && resultCode == Activity.RESULT_OK){
            if(newdate != null){
                mDob.setText( Date_text_formatter(newdate) );
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

       // overridePendingTransition(R.anim.activity_back_in,R.anim.activity_back_out);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {

            case android.R.id.home :

                //overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);

        }
        return super.onOptionsItemSelected(item);
    }

    public static  class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {


        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // use the current date as the default date
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(),this,year,month,day);
        }

        @Override
        public void onDateSet(android.widget.DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            newdate = new GregorianCalendar(year, monthOfYear, dayOfMonth).getTime();

            mDob.setText(Date_text_formatter(newdate));
        }
    }
}
