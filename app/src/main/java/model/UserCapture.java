package model;


/**
 * Created by user pc on 3/8/2018.
 */

public class UserCapture {
    private int userId;
    private byte[] fingerPrint;
    private String fingerPosition;
    private byte[] fingerPrintToSend;

    public UserCapture() {
    }

    public UserCapture(int userId, byte[] fingerPrint, String fingerPosition, byte[] fingerPrintToSend) {
        this.userId = userId;
       this.fingerPrint = fingerPrint;
       this.fingerPosition = fingerPosition;
        this.fingerPrintToSend = fingerPrintToSend;
    }

    public byte[] getFingerPrintToSend() { return fingerPrintToSend; }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public byte[] getFingerPrint() {
        return fingerPrint;
    }

    public void setFingerPrint(byte[] fingerPrint) {
        this.fingerPrint = fingerPrint;
    }

    public String getFingerPosition() {
        return fingerPosition;
    }

    public void setFingerPosition(String fingerPosition) {
        this.fingerPosition = fingerPosition;
    }

}
