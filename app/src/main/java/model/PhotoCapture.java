package model;

import android.graphics.Bitmap;

/**
 * Created by sp_developer on 4/26/16.
 */
public class PhotoCapture {

    private Bitmap photo;

    public PhotoCapture(){

    }

    public Bitmap getPhoto() {
        return photo;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }


}
