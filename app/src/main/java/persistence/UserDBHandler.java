package persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;



import model.UserCapture;

/**
 * Created by user pc on 3/8/2018.
 */

public class UserDBHandler extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "userIDManager.db";

    // user table name
    private static final String TABLE_NAME = "user";

    // Table Columns names
    private static final String KEY_COLUMN_ID = "ID";
    private static final String COLUMN_USER_ID = "user_id";
    private static final String COLUMN_FINGERPRINT = "fingerPrint";
    private static final String COLUMN_FINGERPOSITION = "fingerPosition";
    private static final String COLUMN_FINGERPRINTTOSEND = "fingerPrintToSend";

    //data from the database

    private Context dataBaseContext;

    private SQLiteDatabase database;

    public UserDBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        dataBaseContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ");
        sb.append(TABLE_NAME+" (\n"+KEY_COLUMN_ID);
        sb.append(" INTEGER PRIMARY KEY, ");
        sb.append(COLUMN_USER_ID +" INT, ");
        sb.append(COLUMN_FINGERPRINT+" BLOB, ");
        sb.append(COLUMN_FINGERPOSITION+" TEXT, ");
        sb.append(COLUMN_FINGERPRINTTOSEND+" BLOB\n);");
        String query = sb.toString();
        sqLiteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public void insertRecord(UserCapture userCapture){
        database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_ID, userCapture.getUserId());
        values.put(COLUMN_FINGERPRINT, userCapture.getFingerPrint());
        values.put(COLUMN_FINGERPOSITION, userCapture.getFingerPosition());
        values.put(COLUMN_FINGERPRINTTOSEND, userCapture.getFingerPrintToSend());
        database.insert(TABLE_NAME, null, values);
        Log.d("Info", values.toString());
        database.close();
        Log.d("Successfully inserted ", values.toString());
        Toast.makeText(dataBaseContext, "Insertion was successful", Toast.LENGTH_SHORT).show();
//        Toast.makeText(dataBaseContext, "Successfully inserted"+values.toString(), Toast.LENGTH_LONG).show();
    }



    public ArrayList<UserCapture> getAllCaptures(){

        ArrayList<UserCapture> captureList = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if(cursor.moveToNext()){
            do{
                UserCapture userCapture =  new UserCapture();
                userCapture.setUserId(cursor.getInt(1));
                userCapture.setFingerPrint(cursor.getBlob(2));
                userCapture.setFingerPosition(cursor.getString(3));
//                userCapture.setFingerBase(cursor.getString(4));
                // Adding names to list
                captureList.add(userCapture);

            } while(cursor.moveToNext());
        }

        return  captureList;

    }

    public void deleteDB(){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
    }

    public byte[] getFingerPrintByte(String fingerPosition){
        byte[] fingerPos = null;
        String selectQuery = "SELECT "+COLUMN_FINGERPRINT+" FROM " + TABLE_NAME +" WHERE "+COLUMN_FINGERPOSITION + " = '"+fingerPosition+"'";
        SQLiteDatabase db = UserDBHandler.this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToNext()){
            do{
               fingerPos = cursor.getBlob(2);
            } while(cursor.moveToNext());
        }
        db.close();

        return  fingerPos;
    }

    public UserCapture getFingerCapture(String fingerPosition){
        UserCapture captureObj = null;
        SQLiteDatabase db = UserDBHandler.this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, new String[]{KEY_COLUMN_ID, COLUMN_USER_ID,
                        COLUMN_FINGERPRINT, COLUMN_FINGERPOSITION, COLUMN_FINGERPRINTTOSEND}, COLUMN_FINGERPOSITION + "=?",
                new String[]{fingerPosition}, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                captureObj = new UserCapture(
                        cursor.getInt(1),
                        cursor.getBlob(2),
                        cursor.getString(3),
                        cursor.getBlob(4)
                );

            } while (cursor.moveToNext());
        }
        return captureObj;
    }

}
